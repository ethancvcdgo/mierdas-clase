package DOMJudge;

import java.util.Scanner;

public class dibujarrombosconasteriscos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int n = sc.nextInt();
			for (int j = 1; j <= n; j++) {
				for (int p = n-1; p >= j; p--) {
					System.out.print(" ");
				}
				for (int p = 0; p < j*2-1; p++) {
					System.out.print("*");
				}
				System.out.println();
			}
			for (int j = n-1; j >= 0; j--) {
				for (int p = j; p <= n-1; p++) {
					System.out.print(" ");
				}
				for (int p = j*2-1; p > 0; p--) {
					System.out.print("*");
				}
				System.out.println();
			}
			
		}

	}

}
