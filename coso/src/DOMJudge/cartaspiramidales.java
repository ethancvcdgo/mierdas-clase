package DOMJudge;

import java.util.Scanner;

public class cartaspiramidales {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int j = 0; casos > j; j++) {
			int cartas = sc.nextInt();
			int i = 0;
			int k = 1;
			while (cartas >= i) {
				cartas = cartas - i;
				i = 2 * k + (k - 1);
				k++;
			}
			k = k - 2;
			System.out.println(k + " " + cartas);

		}

	}

}
