package DOMJudge;

import java.util.Scanner;

public class DNI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			String DNI = sc.next();
			int longitud = DNI.length();
			String digito = DNI.substring(0, longitud - 1);
			int ndigito = Integer.parseInt(digito);
			int residu = ndigito % 23;
			char letra = DNI.charAt(longitud - 1);
			switch (residu) {
			case 0:
				if (letra == 'T') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 1:
				if (letra == 'R') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 2:
				if (letra == 'W') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 3:
				if (letra == 'A') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 4:
				if (letra == 'G') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 5:
				if (letra == 'M') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 6:
				if (letra == 'Y') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 7:
				if (letra == 'F') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 8:
				if (letra == 'P') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 9:
				if (letra == 'D') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 10:
				if (letra == 'X') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 11:
				if (letra == 'B') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 12:
				if (letra == 'N') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 13:
				if (letra == 'J') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 14:
				if (letra == 'Z') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 15:
				if (letra == 'S') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 16:
				if (letra == 'Q') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 17:
				if (letra == 'V') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 18:
				if (letra == 'H') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 19:
				if (letra == 'L') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 20:
				if (letra == 'C') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 21:
				if (letra == 'K') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			case 22:
				if (letra == 'E') {
					System.out.println("valid");
				} else {
					System.out.println("invalid");
				}
				break;
			}

		}

	}

}
