package DOMJudge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class intespanola {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			ArrayList<String> lista1 = new ArrayList<String>();
			ArrayList<String> lista2 = new ArrayList<String>();
			ArrayList<String> lista3 = new ArrayList<String>();
			int nnombres = sc.nextInt();
			sc.nextLine();
			String palabra = sc.nextLine();
			for (int j = 1; j < nnombres; j++) {
				lista1.clear();
				lista2.clear();
				String palabro = sc.nextLine();
				for (int k = 0; k < palabro.length(); k++) {
					String letra = palabro.substring(k, k + 1);
					lista1.add(letra);
				}
				for (int k = 0; k < palabra.length(); k++) {
					String letra = palabra.substring(k, k + 1);
					lista2.add(letra);
				}
				Collections.sort(lista2);
				Collections.sort(lista1);
				if (j == 1) {
					lista3.addAll(lista1);
				}
			}
			for (int k = 0; k < lista2.size(); k++) {
				if (nnombres <= 2) {
					if (lista1.contains(lista2.get(k)) && nnombres <= 2) {
						System.out.print(lista2.get(k));
					}
				} else {
					if (lista1.contains(lista2.get(k)) && lista3.contains(lista2.get(k))) {
						System.out.print(lista2.get(k));
					}
				}
			}
			System.out.println();
		}
	}

}
