package DOMJudge;

import java.util.Scanner;

public class oso {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		boolean salir = false;
		while (!salir) {
			int oso = 0;
			int casos = sc.nextInt();
			sc.nextLine();
			if (casos == 0) {
				salir = true;
			} else {
				char[][] osos = new char[casos][casos];
				for (int i = 0; i < osos.length; i++) {
					String linia = sc.nextLine();
					for (int k = 0; k < osos[i].length; k++) {
						osos[i][k] = linia.charAt(k);
					}
				}
				for (int j = 0; j < osos.length; j++) {
					for (int k = 0; k < osos[0].length; k++) {
						if (k >= 1 && k != osos.length - 1) {
							if (osos[j][k] == 'S' && osos[j][k - 1] == 'O' && osos[j][k + 1] == 'O') {
								oso++;
							}
						}

						if (j >= 1 && j != osos.length - 1) {
							if (osos[j][k] == 'S' && osos[j - 1][k] == 'O' && osos[j + 1][k] == 'O') {
								oso++;
							}
						}
						if (k >= 1 && k != osos.length - 1 && j >= 1 && j != osos.length - 1) {
							if ((osos[j][k] == 'S' && osos[j - 1][k - 1] == 'O' && osos[j + 1][k + 1] == 'O')) {
								oso++;
							}
						}
						if (k >= 1 && k != osos.length - 1 && j >= 1 && j != osos.length - 1) {
							if ((osos[j][k] == 'S' && osos[j + 1][k - 1] == 'O' && osos[j - 1][k + 1] == 'O')) {
								oso++;
							}
						}
					}
				}
				System.out.println(oso);
			}
		}
	}
}
