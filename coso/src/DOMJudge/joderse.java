package DOMJudge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class joderse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i < casos; i++) {
			ArrayList<String> lista1 = new ArrayList<String>();
			ArrayList<String> lista2 = new ArrayList<String>();
			String pal1 = sc.nextLine();
			String pal2 = sc.nextLine();
			if (pal1.length() == pal2.length() && pal1.length() % 2 == 0) {
				for (int j = 0; j < pal1.length(); j = j + 2) {
					String silaba = pal1.substring(j, j + 2);
					lista1.add(silaba);
				}
				for (int j = 0; j < pal2.length(); j = j + 2) {
					String silaba = pal2.substring(j, j + 2);
					lista2.add(silaba);
				}
				Collections.reverse(lista1);
				if (lista1.equals(lista2)) {
					System.out.println("SI");
				} else {
					System.out.println("NO");
				}
			} else {
				System.out.println("NO");
			}

		}

	}

}
