package DOMJudge;

import java.util.ArrayList;
import java.util.Scanner;

public class torniquete {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int cont=0;
			String alumnum ="0";
			ArrayList<String> lista = new ArrayList<String>();
			boolean flag = false;
			boolean duplicado = false;
			boolean salir = false;
			int nalum = sc.nextInt();
			sc.nextLine();
			while (!flag) {
				
				String alum = sc.nextLine();
				if (alum.equals("E 0")) {
					flag = true;
				} else if (duplicado==false&&salir==false){
					if (lista.contains(alum)) {
						duplicado = true;
					}
					lista.add(alum);
					alumnum = alum.substring(2, alum.length());
					if (!lista.contains("E " + alumnum) && lista.contains("S " + alumnum)) {
						salir = true;
					}
					cont++;
				}

			}
			if (salir) {
				System.out.println(cont+" SortidaSenseEntrada");
			} else if (duplicado) {
				System.out.println(cont+" EntradaDuplicada");
			} else {
				System.out.println(cont+" Correcte");
			}
		}
	}

}
