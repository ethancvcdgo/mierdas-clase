package DOMJudge;

import java.util.Scanner;

public class ligas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int equipos = sc.nextInt();
			int victorias = sc.nextInt();
			int minimo = (equipos - 1) * victorias;
			int maximo = (equipos - 1) * (victorias * 2 - 1);
			System.out.println(minimo + " " + maximo);
		}
	}

}
