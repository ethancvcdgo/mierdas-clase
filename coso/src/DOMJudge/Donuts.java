package DOMJudge;

import java.util.Scanner;

public class Donuts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner ingreso = new Scanner(System.in);
		int casos = ingreso.nextInt();
		for (int c = 0; c < casos; c++) {
			int mayor = 0, numerador = 0, contador = 0;
			int interruptor = 0, multiplicador = 0, multiplo = 0;
			int i, j;
			int num[] = new int[3];

			for (i = 0; i < 3; i++) {
				numerador++;
				num[i] = ingreso.nextInt();
				if (num[i] > mayor) {
					mayor = num[i];
				}
			}

			while (interruptor == 0) {
				contador = 0;
				multiplicador++;
				multiplo = mayor * multiplicador;
				for (j = 0; j < 3; j++) {
					if (multiplo % num[j] == 0) {
						contador++;
					}
				}
				if (contador == 3) {
					interruptor = 1;
				}
			}

			System.out.println(multiplo);
		}

	}

}
