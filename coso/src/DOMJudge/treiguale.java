package DOMJudge;

import java.util.ArrayList;
import java.util.Scanner;

public class treiguale {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		int nume = 0;
		for (int i = 0; i < casos; i++) {
			ArrayList<Integer> lista = new ArrayList<Integer>();
			for (int j = 0; j < 5; j++) {
				int num = sc.nextInt();
				lista.add(num);
			}
			int acc=0;
			int cos=lista.get(0);
			for (int k = 0; k < lista.size(); k++) {
				if (cos==lista.get(k)) {
					acc++;
					if (acc >= 3) {
						nume = lista.get(k);
					}
				}
			}

			if (acc >= 3) {
				System.out.println(nume);
			} else {
				System.out.println("NO");
			}
		}
	}

}
