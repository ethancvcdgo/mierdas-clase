package funciones;

import java.util.Scanner;

import funciones.Board;
import funciones.Window;

public class submarinos {
	static int[][] barcos = { { 2, 0, 1, 1, 0, 0, 0, 0, 0, 0 }, { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 4, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 4, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 4, 0 }, { 0, 0, 3, 3, 3, 3, 0, 0, 4, 0 },
			{ 0, 0, 0, 0, 5, 0, 0, 0, 4, 0 }, { 0, 0, 0, 5, 5, 5, 0, 0, 0, 0 } };
	static int[][] explo;
	static Board f = new Board();
	static Window w = new Window(f);

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		boolean partida = true;
		init();
		while (partida) {
			int x = f.getCurrentMouseRow();
			int y = f.getCurrentMouseCol();
			Thread.sleep(50);
			if (x != -1 && y != -1) {
				System.out.println(x + " " + y);
				diparar(x, y);
			}
			f.draw(explo, 'c');
			partida = partido();
		}
		System.out.println("Sacabao");
	}

	private static boolean partido() {
		// TODO Auto-generated method stub
		for (int i = 0; i < barcos.length; i++) {
			for (int j = 0; j < barcos[0].length; j++) {
				if (barcos[i][j] != 0) {
					return true;
				}
			}
		}
		return false;
	}

	private static void diparar(int x, int y) {
		// TODO Auto-generated method stub
		if (barcos[x][y] != 0) {
			System.out.println("Tocado");
			int i = barcos[x][y];

			if (x != 0 && x != 9 && y != 0 && y != 9) {
				if (barcos[x - 1][y] != i && barcos[x + 1][y] != i && barcos[x + 1][y + 1] != i
						&& barcos[x - 1][y + 1] != i && barcos[x + 1][y - 1] != i && barcos[x][y + 1] != i
						&& barcos[x][y - 1] != i) {
					System.out.println("Hundido");
				}
			}
			barcos[x][y] = 0;
			explo[x][y] = 1;
		} else {
			System.out.println("AwA");
			explo[x][y] = 2;
		}
	}

	private static void init() {
		// TODO Auto-generated method stub
		explo = new int[10][10];
		int[] colors = { 0x0000FF, 0xFF0000, 0x00FFFF };
		f.setActborder(true);
		f.setColors(colors);
		f.draw(explo, 'c');
	}
}
