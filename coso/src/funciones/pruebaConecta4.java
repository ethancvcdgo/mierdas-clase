package funciones;

import java.util.Scanner;

public class pruebaConecta4 {
	static Scanner sc = new Scanner(System.in);
	static int[][] tablero;
	static int torn;
	static int partida;
	static String jug1 = "1";
	static String jug2 = "2";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean salir = false;
		while (!salir) {
			int num = sc.nextInt();
			sc.nextLine();
			switch (num) {
			case 1:
				// Instruccions
				instruccions();
				break;
			case 2:
				// Config
				config();
				break;
			case 3:
				// Jugar
				jugar();
				break;
			case 0:
				salir = true;
				break;
			}
		}

	}

	private static void jugar() {
		// TODO Auto-generated method stub
		init();
		visualitza();
		while (partida == 3) {
			ponerFicha();
			visualitza();
			partida = comprobar();
			torn++;
		}
		fini();
	}

	private static void ponerFicha() {
		// TODO Auto-generated method stub
		int col;
		int i = 0;
		if (torn % 2 == 0) {
			// jugador 1
			System.out.println(jug1 + " elige columna");
			col = sc.nextInt();
			while (tablero[i + 1][col] == 0) {
				i++;
			}
			tablero[i][col] = 1;
		} else {
			// jugador 2
			System.out.println(jug2 + " elige columna");
			col = sc.nextInt();
			while (tablero[i + 1][col] == 0) {
				i++;
			}
			tablero[i][col] = 2;
		}
	}

	private static int comprobar() {
		// TODO Auto-generated method stub
		if (tablero[0][0] == 1 && tablero[0][1] == 1 && tablero[0][2] == 1 && tablero[0][3] == 1
				|| tablero[1][0] == 1 && tablero[1][1] == 1 && tablero[1][2] == 1 && tablero[1][3] == 1
				|| tablero[2][0] == 1 && tablero[2][1] == 1 && tablero[2][2] == 1 && tablero[2][3] == 1
				|| tablero[3][0] == 1 && tablero[3][1] == 1 && tablero[3][2] == 1 && tablero[3][3] == 1
				|| tablero[0][0] == 1 && tablero[1][0] == 1 && tablero[2][0] == 1 && tablero[3][0] == 1
				|| tablero[0][1] == 1 && tablero[1][1] == 1 && tablero[2][1] == 1 && tablero[3][1] == 1
				|| tablero[0][2] == 1 && tablero[1][2] == 1 && tablero[2][2] == 1 && tablero[3][2] == 1
				|| tablero[0][3] == 1 && tablero[1][3] == 1 && tablero[2][3] == 1 && tablero[3][3] == 1
				|| tablero[0][0] == 1 && tablero[1][1] == 1 && tablero[2][2] == 1 && tablero[3][3] == 1
				|| tablero[3][0] == 1 && tablero[2][1] == 1 && tablero[1][2] == 1 && tablero[0][3] == 1) {
			return 0;
		} else if (tablero[0][0] == 2 && tablero[0][1] == 2 && tablero[0][2] == 2 && tablero[0][3] == 2
				|| tablero[1][0] == 2 && tablero[1][1] == 2 && tablero[1][2] == 2 && tablero[1][3] == 2
				|| tablero[2][0] == 2 && tablero[2][1] == 2 && tablero[2][2] == 2 && tablero[2][3] == 2
				|| tablero[3][0] == 2 && tablero[3][1] == 2 && tablero[3][2] == 2 && tablero[3][3] == 2
				|| tablero[0][0] == 2 && tablero[1][0] == 2 && tablero[2][0] == 2 && tablero[3][0] == 2
				|| tablero[0][1] == 2 && tablero[1][1] == 2 && tablero[2][1] == 2 && tablero[3][1] == 2
				|| tablero[0][2] == 2 && tablero[1][2] == 2 && tablero[2][2] == 2 && tablero[3][2] == 2
				|| tablero[0][3] == 2 && tablero[1][3] == 2 && tablero[2][3] == 2 && tablero[3][3] == 2
				|| tablero[0][0] == 2 && tablero[1][1] == 2 && tablero[2][2] == 2 && tablero[3][3] == 2
				|| tablero[3][0] == 2 && tablero[2][1] == 2 && tablero[1][2] == 2 && tablero[0][3] == 2) {
			return 1;
		} else if (tablero[0][0] != 0 && tablero[0][1] != 0 && tablero[0][2] != 0 && tablero[0][3] != 0
				&& tablero[1][0] != 0 && tablero[1][1] != 0 && tablero[1][2] != 0 && tablero[1][3] != 0
				&& tablero[2][0] != 0 && tablero[2][1] != 0 && tablero[2][2] != 0 && tablero[2][3] != 0
				&& tablero[3][0] != 0 && tablero[3][1] != 0 && tablero[3][2] != 0 && tablero[3][3] != 0) {
			return 2;
		} else {
			return 3;
		}
	}

	private static void visualitza() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j]);
			}
			System.out.println();
		}
	}

	private static void fini() {
		// TODO Auto-generated method stub
		if (partida == 0) {
			System.out.println("Gana " + jug1);
		} else if (partida == 1) {
			System.out.println("Gana " + jug2);
		} else if (partida == 2) {
			System.out.println("Empate");
		}
	}

	private static void init() {
		// TODO Auto-generated method stub
		tablero = new int[4][4];
		partida = 3;
	}

	private static void config() {
		// TODO Auto-generated method stub
		// Nom jug1 i jug2
		System.out.println("Nom primer jugador");
		jug1 = sc.nextLine();
		System.out.println("Nom segon jugador");
		jug2 = sc.nextLine();

	}

	private static void instruccions() {
		// TODO Auto-generated method stub
		System.out.println(
				"El connecta 4 es un joc a on s’han de posar en linea 4 fitxes del mateix color. Per torns els jugadors introdueixen una fitxa en la columna que vulguin i aquesta caurà a la posició més baixa. Guanya la partida el primer en alinear 4 fitxes del mateix color. Si totes les columnes queden plenes però ningú ha guanyat es dona empat.\n");
	}
}
