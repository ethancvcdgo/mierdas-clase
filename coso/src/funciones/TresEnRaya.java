package funciones;

import java.util.Scanner;

public class TresEnRaya {

	// variable global: se usa en mas de una funcion, se declara fuera y estatica
	static String[][] tablero;
	static boolean turnob;
	static int turnoi;
	static char turnoc;
	static String turnos;

	public static void main(String[] args) {

		// siempre vamos a tener una funcion de inicializacion
		init();
		// un turno
		int arcaico = 3;
		while (arcaico == 3) {
			ponerFicha(turnos);
			mostrarTablero();
			arcaico = comprobarVictoria();
			// turnob = cambiarTurnob(turnob);
			// turnoi = cambiarTurnoi(turnoi);
			// turnoc = cambiarTurnoc(turnoc);
			turnos = cambiarTurnos(turnos);
		}

	}

	private static void mostrarTablero() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j]);
			}
			System.out.println();
		}
	}

	private static void init() {
		// TODO Auto-generated method stub
		tablero = new String[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				tablero[i][j] = "O";
			}
		}
		turnos = "卐";
	}

	private static void ponerFicha(String turnos2) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Juega " + turnos2);
		int fil = sc.nextInt();
		int col = sc.nextInt();
		if (tablero[fil][col].equals("O")) {
			if (turnos2.equals("卐")) {
				tablero[fil][col] = "卐";
			} else {
				tablero[fil][col] = "✡";
			}
		} else {
			System.out.println("Ahí ya hay una ficha");
		}

	}

	private static String cambiarTurnos(String turnos2) {
		if (turnos2.equals("卐")) {
			return "✡";
		} else {
			return "卐";
		}
	}

	private static char cambiarTurnoc(char turnoc2) {
		if (turnoc2 == 'X') {
			return 'O';
		} else {
			return 'X';
		}
	}

	private static int cambiarTurnoi(int turnoi2) {
		return (turnoi2 + 1) % 2;
	}

	private static boolean cambiarTurnob(boolean turnob2) {
		return !turnob;
	}

	private static int comprobarVictoria() {
		// TODO
		if (tablero[0][0].equals("卐") && tablero[0][0] == tablero[0][1] && tablero[0][1] == tablero[0][2]
				|| tablero[1][0].equals("卐") && tablero[1][0] == tablero[1][1] && tablero[1][1] == tablero[1][2]
				|| tablero[2][0].equals("卐") && tablero[2][0] == tablero[2][1] && tablero[2][1] == tablero[2][2]
				|| tablero[0][0].equals("卐") && tablero[0][0] == tablero[1][0] && tablero[1][0] == tablero[2][0]
				|| tablero[0][1].equals("卐") && tablero[0][1] == tablero[1][1] && tablero[1][1] == tablero[2][1]
				|| tablero[0][2].equals("卐") && tablero[0][2] == tablero[1][2] && tablero[1][2] == tablero[2][2]
				|| tablero[0][0].equals("卐") && tablero[0][0] == tablero[1][1] && tablero[1][1] == tablero[2][2]
				|| tablero[0][2].equals("卐") && tablero[0][2] == tablero[1][1] && tablero[1][1] == tablero[2][0]) {
			System.out.println("Gana 卐");
			return 0;
		} else if (tablero[0][0].equals("✡") && tablero[0][0] == tablero[0][1] && tablero[0][1] == tablero[0][2]
				|| tablero[1][0].equals("✡") && tablero[1][0] == tablero[1][1] && tablero[1][1] == tablero[1][2]
				|| tablero[2][0].equals("✡") && tablero[2][0] == tablero[2][1] && tablero[2][1] == tablero[2][2]
				|| tablero[0][0].equals("✡") && tablero[0][0] == tablero[1][0] && tablero[1][0] == tablero[2][0]
				|| tablero[0][1].equals("✡") && tablero[0][1] == tablero[1][1] && tablero[1][1] == tablero[2][1]
				|| tablero[0][2].equals("✡") && tablero[0][2] == tablero[1][2] && tablero[1][2] == tablero[2][2]
				|| tablero[0][0].equals("✡") && tablero[0][0] == tablero[1][1] && tablero[1][1] == tablero[2][2]
				|| tablero[0][2].equals("✡") && tablero[0][2] == tablero[1][1] && tablero[1][1] == tablero[2][0]) {
			System.out.println("Gana ✡");
			return 1;
		} else if (!tablero[0][0].equals("O") && !tablero[0][1].equals("O") && !tablero[0][2].equals("O")
				&& !tablero[1][0].equals("O") && !tablero[1][1].equals("O") && !tablero[1][2].equals("O")
				&& !tablero[2][0].equals("O") && !tablero[2][1].equals("O") && !tablero[2][2].equals("O")) {
			System.out.println("Empate");
			return 2;
		} else {
			return 3;
		}

	}

}
