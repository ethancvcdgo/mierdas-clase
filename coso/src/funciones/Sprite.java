package funciones;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Sprite {
	
	public String name;
	
	public int x1;
	public int y1;
	
	public int x2;
	public int y2;
	
	public String path;
	public boolean solid;
	
	
	public Image img; 
	
	public Sprite(String name, int x1, int y1, int x2, int y2, String path) {
		super();
		this.name = name;
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.path = path;
		img = new ImageIcon((this.path)).getImage();
	}
	
	protected void renew() {
		//TODO hacerlo automatico
		img = new ImageIcon((this.path)).getImage();
	}
	
    protected Rectangle getRect() {
        return new Rectangle(x1, y1, x2-x1, y2-y1);
    }
    
    public ArrayList<Sprite> collidesWithList(ArrayList<? extends Sprite> others) {
    	ArrayList<Sprite> list = new ArrayList<>();
    	for(Sprite s: others) {
    		if (this.getRect().intersects(s.getRect())) list.add(s);
    	}
    	return list;
    	
    }
    
    public boolean collidesWith(Sprite other) {
    	return (this.getRect().intersects(other.getRect()) )?  true : false;
    }
    
    public Sprite firstCollidesWithList(ArrayList<? extends Sprite> others) {
    	for(Sprite s: others) {
    		if (this.getRect().intersects(s.getRect())) return s;
    	}
    	return null;
    	
    }
	
	
	

}
