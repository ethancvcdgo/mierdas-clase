package funciones;

import java.util.Random;
import java.util.Scanner;

public class MasterMind {
	static Scanner sc = new Scanner(System.in);
	static int[] secret = new int[4];
	static int[] answer = new int[4];
	static char[] compa = new char[4];

	public static void main(String[] args) {
		boolean winner = false;
		init();
		while (!winner) {
			contestar();
			comprobar();
			System.out.println(compa);
			winner=victoria();
		}
	}

	private static boolean victoria() {
		// TODO Auto-generated method stub
		if(secret[0]==answer[0]&&secret[1]==answer[1]&&secret[2]==answer[2]&&secret[3]==answer[3]) {
			return true;
		}else {
		return false;
		}
	}

	private static void comprobar() {
		// TODO Auto-generated method stub
		if (answer[0] == secret[0]) {
			compa[0] = (char) answer[0];
		} else if(answer[0]==secret[1]||answer[0]==secret[2]||answer[0]==secret[3]){
			System.out.println("Primero mal sitio");
			compa[0] = 'X';
		}else {
			compa[0] = 'X';
		}
		if (answer[1] == secret[1]) {
			compa[1] = (char) answer[1];
		}  else if(answer[1]==secret[0]||answer[1]==secret[2]||answer[1]==secret[3]){
			System.out.println("Segundo mal sitio");
			compa[1] = 'X';
		}else {
			compa[1] = 'X';
		}
		if (answer[2] == secret[2]) {
			compa[2] = (char) answer[2];
		}  else if(answer[2]==secret[1]||answer[2]==secret[0]||answer[2]==secret[3]){
			System.out.println("Tercero mal sitio");
			compa[2] = 'X';
		}else {
			compa[2] = 'X';
		}
		if (answer[3] == secret[3]) {
			compa[3] = (char) answer[3];
		}  else if(answer[3]==secret[1]||answer[3]==secret[0]||answer[3]==secret[2]){
			System.out.println("Cuarto mal sitio");
			compa[3] = 'X';
		}else {
			compa[3] = 'X';
		}
	}

	private static void contestar() {
		// TODO Auto-generated method stub
		System.out.println("introduce combinacion");
		answer[0] = sc.nextInt();
		answer[1] = sc.nextInt();
		answer[2] = sc.nextInt();
		answer[3] = sc.nextInt();
	}

	private static void init() {
		// TODO Auto-generated method stub
		Random r = new Random();
		secret[0] = r.nextInt(9)+1;
		secret[1] = r.nextInt(9)+1;
		secret[2] = r.nextInt(9)+1;
		secret[3] = r.nextInt(9)+1;
	}

}
