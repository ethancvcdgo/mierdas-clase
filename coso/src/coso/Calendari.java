package coso;

import java.util.Scanner;

public class Calendari {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner SCA = new Scanner(System.in);
		System.out.println("Escriu dia y número de mes");
		int dia = SCA.nextInt();
		int mes = SCA.nextInt();

		if (mes == 2) {

			if (dia > 28) {
				System.out.println("ERROR");
			} else {
				System.out.println("Falten " + (28 - dia) + " dies");
			}

		} else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {

			if (dia > 31) {
				System.out.println("ERROR");
			} else {
				System.out.println("Falten " + (31 - dia) + " dies");
			}

		} else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			if (dia > 30) {
				System.out.println("ERROR");
			} else {
				System.out.println("Falten " + (30 - dia) + " dies");
			}

		} else {
			System.out.println("ERROR");
		}

	}

}
