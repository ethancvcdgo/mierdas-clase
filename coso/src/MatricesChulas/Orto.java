package MatricesChulas;

import java.util.Random;
import java.util.Scanner;

///Joc amb un taulell 2D
public class Orto {

	static int naziy;
	static int nazix;
	static int x;
	static int y;
	static int[][] tablero;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		tablero = new int[7][7];

		Board b = new Board();
		Window w = new Window(b);

		b.setActcolors(false);
		// activar modo sprites
		b.setActsprites(true);
		b.setActimgbackground(true);
		// creo el vector de sprites a mano
		String[] sprites = { "", "char.png", "calabaza.png", "bugero.png", "", "", "", "nz.png" };
		// le paso el vector que acabo de crear al motor
		b.setSprites(sprites);
		// le paso al motor un fondo
		b.setImgbackground("ger.png");

		Utils.view(tablero);
		b.draw(tablero);

		Random r = new Random();
		/// personaje sera un 1.
		nazix = r.nextInt(7);
		naziy = r.nextInt(7);

		int calabazas = 0;
		x = 3;
		y = 3;
		tablero[x][y] = 1;
		tablero[nazix][naziy] = 7;
		tablero[r.nextInt(7)][r.nextInt(7)] = 2;
		tablero[r.nextInt(7)][r.nextInt(7)] = 3;

		Utils.view(tablero);
		b.draw(tablero);

		int turno = 0;

		boolean sortir = false;
		while (!sortir) {
			char opt = sc.nextLine().charAt(0);
			switch (opt) {
			case 'w':
				if (x == 0) {
					System.out.println("te vas a caer por el borde de la tierra plana");
				} else {
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					x--;
					if (tablero[x][y] == 2) {
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
						
					}
					if (tablero[x][y] == 7 || tablero[x][y] == 3) {
						System.out.println("pa'l lobby");
						sortir = true;
						break;
					}
					tablero[x][y] = 1;
				}
				break;
			case 'a':
				if (y == 0) {
					System.out.println("te vas a caer por el borde de la tierra plana");
				} else {
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y--;
					if (tablero[x][y] == 2) {
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7 || tablero[x][y] == 3) {
						System.out.println("pa'l lobby");
						sortir = true;
						break;
					}
					tablero[x][y] = 1;
				}
				break;
			case 's':
				if (x == tablero.length - 1) {
					System.out.println("te vas a caer por el borde de la tierra plana");
				} else {
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					x++;
					if (tablero[x][y] == 2) {
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7 || tablero[x][y] == 3) {
						System.out.println("pa'l lobby");
						sortir = true;
						break;
					}
					tablero[x][y] = 1;
				}
				break;
			case 'd':
				if (y == tablero[0].length - 1) {
					System.out.println("te vas a caer por el borde de la tierra plana");
				} else {
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y++;
					if (tablero[x][y] == 2) {
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7 || tablero[x][y] == 3) {
						System.out.println("pa'l lobby");
						sortir = true;
						break;
					}
					tablero[x][y] = 1;
				}
				break;
			case 'p':
				System.out.println("has salvado " + calabazas + " veces a tu gente");
				sortir = true;
				break;
			default:
				break;
			}
			if (tablero[nazix][naziy] != 3 && turno % 2 == 0) {
				movimienton();
			}
			turno++;
			
			Utils.view(tablero);
			b.draw(tablero);
		}

	}

	private static void movimienton() {
		// TODO movimiento ciudadano
		int distx = x - nazix;
		int disty = y - naziy;
		tablero[nazix][naziy] = 0;

		if (Math.abs(distx) > Math.abs(disty)) {
			if (distx > 0) {
				nazix++;
			} else {
				nazix--;
			}
		} else {
			if (disty > 0) {
				naziy++;
			} else {
				naziy--;
			}
		}
		if (tablero[nazix][naziy] == 1) {
			System.out.println("tremendo cogidon en la funcion movimienton");
		}
		if (tablero[nazix][naziy] == 3) {
			tablero[nazix][naziy] = 3;
		} else {
			tablero[nazix][naziy] = 7;
		}

	}

}
