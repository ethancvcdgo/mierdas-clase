package MatricesChulas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class Simon {

	public static void main(String[] args) throws InterruptedException {

		int[][] simon = { { 1, 2 }, { 3, 4 } };

		Board b = new Board();
		Window w = new Window(b);

		b.setActcolors(false);
		// activar modo sprites
		b.setActsprites(true);
		b.setActimgbackground(true);
		// creo el vector de sprites a mano
		String[] sprites = { "", "ger.png", "comunism.png", "bugero.png", "taco.jpeg", "tenor.jpg", "stall.jpg", "mus.jpg", "pum.jpg" };
		// le paso el vector que acabo de crear al motor
		b.setSprites(sprites);
		// le paso al motor un fondo
		b.setImgbackground("tenor.gif");

		/// Simon dice

		Utils.view(simon);
		b.draw(simon);
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		ArrayList<Integer> repes = new ArrayList<Integer>();
		boolean loser = false;

		while (!loser) {
			int colorrandom = r.nextInt(4) + 1;
			repes.add(colorrandom);

			for (int color : repes) {

				int x, y;
				switch (color) {
				case 1:
					x = 0;
					y = 0;
					break;
				case 2:
					x = 0;
					y = 1;
					break;
				case 3:
					x = 1;
					y = 0;
					break;
				case 4:
					x = 1;
					y = 1;
					break;
				default:
					x = 0;
					y = 0;

				}

				// eso es lo mismo que simon[x][y] = simon[x][y]+4;
				simon[x][y] += 4;
				Utils.view(simon);
				b.draw(simon);
				Thread.sleep(750);
				simon[x][y] -= 4;
				b.draw(simon);
				Thread.sleep(200);

			}

			System.out.println("ahora juega el jugador");

			int aciertos = 0;
			while (aciertos<repes.size()&&!loser) {
				Thread.sleep(50);
				int row = b.getCurrentMouseRow();
				int col = b.getCurrentMouseCol();
				if (row != -1 && col != -1) {
					System.out.println(row + " " + col);
					int jug=0;
					if(row==0&&col==0) jug = 1;
					if(row==0&&col==1) jug = 2;
					if(row==1&&col==0) jug = 3;
					if(row==1&&col==1) jug = 4;
					if (repes.get(aciertos) != jug) {
						System.out.println("loser");
						loser = true;
						break;
					}else {
						aciertos++;
					}
					
					if (b.getCurrentMouseCol() == 2)
						break;
				}
			}

			
		}

		System.out.println("has perdido en la ronda " + repes.size());

	}

}
